option(command_helloword "Compilation option" ON)
if(command_helloword) 
 add_executable(helloworld helloworld.cpp)
 target_link_libraries(helloworld ultimaille)
endif()

 add_executable(tex_coord tex_coord.cpp)
 target_link_libraries(tex_coord ultimaille)

 add_executable(quality_conformal quality-conformal.cpp)
 target_link_libraries(quality_conformal ultimaille)

