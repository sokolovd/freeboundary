file(REMOVE_RECURSE
  "../lib/libultimaille.pdb"
  "../lib/libultimaille.so"
  "../lib/libultimaille.so.1.0"
  "CMakeFiles/ultimaille.dir/ultimaille/HLBFGS_wrapper.cpp.o"
  "CMakeFiles/ultimaille.dir/ultimaille/assert.cpp.o"
  "CMakeFiles/ultimaille.dir/ultimaille/colocate.cpp.o"
  "CMakeFiles/ultimaille.dir/ultimaille/eigen.cpp.o"
  "CMakeFiles/ultimaille.dir/ultimaille/hboxes.cpp.o"
  "CMakeFiles/ultimaille.dir/ultimaille/io/geogram.cpp.o"
  "CMakeFiles/ultimaille.dir/ultimaille/io/medit.cpp.o"
  "CMakeFiles/ultimaille.dir/ultimaille/io/obj.cpp.o"
  "CMakeFiles/ultimaille.dir/ultimaille/io/vtk.cpp.o"
  "CMakeFiles/ultimaille.dir/ultimaille/pointset.cpp.o"
  "CMakeFiles/ultimaille.dir/ultimaille/polyline.cpp.o"
  "CMakeFiles/ultimaille.dir/ultimaille/surface.cpp.o"
  "CMakeFiles/ultimaille.dir/ultimaille/vec.cpp.o"
  "CMakeFiles/ultimaille.dir/ultimaille/volume.cpp.o"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/ultimaille.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
