#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#undef NDEBUG
#include <cassert>
#include <ultimaille/all.h>

using namespace UM;

double facet_area_2d(const Surface &m, int f) {
    double area = 0;
    for (int v=0; v<m.facet_size(f); v++) {
        vec3 a = m.points[m.vert(f, v)];
        vec3 b = m.points[m.vert(f, (v+1)%m.facet_size(f))];
        area += (b.y-a.y)*(b.x+a.x)/2;
    }
    return area;
}

double triangle_area_3d(Triangles &m, int f) {
    vec3 eij = m.points[m.vert(f, 1)] - m.points[m.vert(f, 0)];
    vec3 eik = m.points[m.vert(f, 0)] - m.points[m.vert(f, 2)];
    return 0.5*cross(eij, eik).norm();
}

double angle_2d(const Surface &m, int f, int lv) {
    int n = m.facet_size(f);
    vec3 a = m.points[m.vert(f, (lv-1+n)%n)] - m.points[m.vert(f, lv)];
    vec3 b = m.points[m.vert(f, (lv+1  )%n)] - m.points[m.vert(f, lv)];
    double angle = atan2(a.y, a.x) - atan2(b.y, b.x);
    if (angle > M_PI)        { angle -= 2 * M_PI; }
    else if (angle <= -M_PI) { angle += 2 * M_PI; }
    return angle;
}

double triangle_area_2d(vec2 a, vec2 b, vec2 c) {
    return .5*((b.y-a.y)*(b.x+a.x) + (c.y-b.y)*(c.x+b.x) + (a.y-c.y)*(a.x+c.x));
}

double project_triangle(const vec3& p0, const vec3& p1, const vec3& p2, vec2& z0, vec2& z1, vec2& z2) {
    vec3 X = (p1 - p0).normalize(); // construct an orthonormal 3d basis
    vec3 Z = cross(X, p2 - p0).normalize();
    vec3 Y = cross(Z, X);

    z0 = vec2(0,0); // project the triangle to the 2d basis (X,Y)
    z1 = vec2((p1 - p0).norm(), 0);
    z2 = vec2((p2 - p0)*X, (p2 - p0)*Y);
    return triangle_area_2d(z0, z1, z2);
}




int main(int argc, char** argv) {
    if (3>argc) {
        std::cerr << "Usage: " << argv[0] << " surface3d.obj flattening.obj" << std::endl;
        return 1;
    }
//    std::cerr << "Checking " << argv[1] << std::endl;

    Triangles m3d, m2d;
    read_by_extension(argv[1], m3d);
    read_by_extension(argv[2], m2d);

    std::cerr << std::setprecision(std::numeric_limits<double>::max_digits10);

    assert(m3d.nfacets()==m2d.nfacets() && m3d.nverts()==m2d.nverts());

    double target_area = 0;
    double source_area = 0;

    for (int t : facet_iter(m3d)) {
        vec3 A = m2d.points[m2d.vert(t, 0)];
        vec3 B = m2d.points[m2d.vert(t, 1)];
        vec3 C = m2d.points[m2d.vert(t, 2)];
        vec2 a = {A.x,A.y};
        vec2 b = {B.x,B.y};
        vec2 c = {C.x,C.y};

        double a2d = triangle_area_2d(a, b, c);
        double a3d = triangle_area_3d(m3d, t);;

//        std::cerr << a << " " << b << " " << c << std::endl;
//      std::cerr << t << " " << a2d << " " << a3d << std::endl;
//      um_assert(a2d>0 && a3d>0);

        target_area += a2d;
        source_area += a3d;
    }

//  for (int t : facet_iter(m3d)) {
//      target_area += triangle_area_3d(m2d, t);
//      source_area += triangle_area_3d(m3d, t);
//  }


    FacetAttribute<double> T(m3d);
    FacetAttribute<double> det(m3d);
    for (int t : facet_iter(m3d)) {
        vec3 pi = m3d.points[m3d.vert(t, 0)];
        vec3 pj = m3d.points[m3d.vert(t, 1)];
        vec3 pk = m3d.points[m3d.vert(t, 2)];
        vec2 zi, zj, zk; // project the triangle to a local 2d basis
        double area = project_triangle(pi, pj, pk, zi, zj, zk);
        if (area<=0) {
            std::cerr << "Error: the reference area must be positive" << std::endl;
            return 1;
        }
        zi = zi*std::sqrt(target_area/source_area);
        zj = zj*std::sqrt(target_area/source_area);
        zk = zk*std::sqrt(target_area/source_area);
        area *= target_area/source_area;
        mat<3,2> ref_tri = mat<3,2>{{ {(zk-zj).y, -(zk-zj).x}, {(zi-zk).y, -(zi-zk).x}, {(zj-zi).y, -(zj-zi).x} }}/(-2.*area);

        mat<2,2> J = {};

        for (int i=0; i<3; i++)
            for (int d : range(2))
                J[d] += ref_tri[i]*m2d.points[m2d.vert(t,i)][d];

        mat2x2 G = J.transpose() * J;
        mat2x2 evec;
        vec2 eval;
        eigendecompose_symmetric(G, eval, evec);
        double smax = std::sqrt(eval.x);
        double smin = std::sqrt(eval.y);
        T[t] = .5*(smax/smin + smin/smax);
        det[t] = J.det();
        std::cerr << T[t] << "," << det[t] << std::endl;
    }

    write_geogram("out.geogram", m3d, { {}, {{"t", T.ptr}, {"det", det.ptr}}, {}});

    return 0;
}

