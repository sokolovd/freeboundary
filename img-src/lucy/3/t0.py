#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker


plt.rcParams["font.family"] = "serif"
plt.rcParams["mathtext.fontset"] = "dejavuserif"
plt.rcParams['text.usetex'] = True
plt.rc('font', size=56)
plt.rcParams['hatch.color'] = 'white'
plt.rcParams['hatch.linewidth'] = 20.0
plt.rcParams['figure.autolayout'] = True
plt.rcParams["legend.framealpha"] = 1.

fig, ax = plt.subplots(1, figsize=(16,9),dpi=200)

[T, D] = np.loadtxt("t0.csv", delimiter=',', skiprows=1, unpack=True)
#T = np.clip(T, a_min = 0, a_max = 10)

plt.yscale('log')

def plot_loghist(x, bins):
  hist, bins = np.histogram(x, bins=bins)
  logbins = np.logspace(np.log10(bins[0]),np.log10(bins[-1]),len(bins))
  plt.hist(x, bins=logbins,log=True)
  plt.xscale('log')

#plot_loghist(T, 100)

plt.hist(T, bins=100)
plt.xlabel(r'$\frac{1}{2}\left(\frac{\sigma_1}{\sigma_2} + \frac{\sigma_2}{\sigma_1}\right)$')
ax.set_xticks([1, 25, 50, 75, 100])

#plt.hist(D, bins=100)
#plt.xlabel(r'det $J$')


plt.ylabel('element count')
ax.set_yticks([10**1, 10**3, 10**5])
#ax.xaxis.set_major_locator(ticker.NullLocator())
#ax.xaxis.set_minor_locator(ticker.NullLocator())
#ax.set_xticks([1., 1.1, 1.25, 1.4])

for tick in ax.xaxis.get_majorticklabels():
    tick.set_y(-.02)

#for tick in ax.xaxis.get_minorticklabels():
#    tick.set_y(+.04)


plt.tight_layout()
plt.savefig("t0.png")
#plt.savefig("dt0.png")
plt.show()

